'use strict';
const {faker} = require("@faker-js/faker")

const users = [...Array(100)].map((user)=> {
  const firstmame = faker.name.firstmame();
  const lastname = faker.name.lastname();
  return {
    firstmame:firstmame,
    lastname: lastname,
    email: faker.internet.email(firstmame, lastname),
    username: faker.internet.username(firstmame, lastname),
    password: faker.internet.password(firstmame, lastname),
    createdAt: new Date(),
    updatedAt: new Date()
  }
})


module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Users", users, {});
  // return queryInterface.bulkInsert("Words",[
  //   {
  //     date:"20221019",
  //     word:"Avion",
  //     createdAt: new Date(),
  //     updatedAt: new Date()
  //   },
  //   {
  //     date:"20221019",
  //     word:"coco",
  //     createdAt: new Date(),
  //     updatedAt: new Date()
  //   },
  //   {
  //     date:"20221019",
  //     word:"Blibli",
  //     createdAt: new Date(),
  //     updatedAt: new Date()
  //   }
  // ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Users", null);

    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
