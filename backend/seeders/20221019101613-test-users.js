'use strict';
const {faker} = require("@faker-js/faker")

const users = [...Array(100)].map((user)=> {
  const firstmame = faker.name.firstmame();
  const lastname = faker.name.lastname();
  return {
    firstmame:firstmame,
    lastname: lastname,
    email: faker.internet.email(firstmame, lastname),
    username: faker.internet.username(firstmame, lastname),
    password: faker.internet.password(firstmame, lastname),
    createdAt: new Date(),
    updatedAt: new Date()
  }
});

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Users", users, {});
 
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Users", null);
   
  }
};
